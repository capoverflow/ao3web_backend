package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/capoverflow/ao3web_backend/database"
	"gitlab.com/capoverflow/ao3web_backend/router"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

func init() {

}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	if err := godotenv.Load(".env"); err != nil {
		panic("Error loading .env file")
	}

	fmt.Println("Hello " + os.Getenv("NAME"))

	app := fiber.New()
	app.Use(cors.New())

	database.ConnectDB()

	router.SetupRoutes(app)
	log.Println(app.Listen(":3000"))
}

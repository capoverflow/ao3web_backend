module gitlab.com/capoverflow/ao3web_backend

go 1.16

// replace gitlab.com/capoverflow/ao3api => ../../ao3api

require (
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/antchfx/xmlquery v1.3.6 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/gofiber/jwt/v2 v2.2.1
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgx/v4 v4.11.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.12.1 // indirect
	github.com/lib/pq v1.10.1 // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	gitlab.com/capoverflow/ao3api v0.0.0-20210325163019-0bf2fe9d33c6
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/net v0.0.0-20210423184538-5f58ad60dda6 // indirect
	golang.org/x/sys v0.0.0-20210423185535-09eb48e85fd7 // indirect
	gorm.io/datatypes v1.0.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.8
)

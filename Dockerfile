FROM alpine:edge AS build

RUN apk update
RUN apk upgrade
RUN apk add go build-base

ENV GOPATH /app

WORKDIR $GOPATH/src/gitlab.com/capoverflow/ao3web

COPY . .

# Download all the dependencies

RUN go get -d -v ./...

# Install the package
RUN CGO_ENABLED=1 GOOS=linux go install -v ./...



FROM alpine:edge
WORKDIR /app
RUN cd /app
COPY --from=build /app/bin/ao3web_backend /app/bin/ao3web_backend

# This container exposes port 3000 to the outside world
EXPOSE 3000

# Run the executable
CMD ["bin/ao3web_backend"]#                                                                                                                                                      ➜  ~ 



package handler

import (
	"encoding/json"
	"log"

	ao3 "gitlab.com/capoverflow/ao3api"
	"gitlab.com/capoverflow/ao3web_backend/database"
	"gitlab.com/capoverflow/ao3web_backend/model"
	"gorm.io/datatypes"

	"github.com/gofiber/fiber/v2"
)

type fanficInfo struct {
	WorkID    string `json:"workid" xml:"workid" form:"workid"`
	ChapterID string `json:"chapterid" xml:"chapterid" form:"chapterid"`
}

// GetAllFanfics query all products
func GetAllFanfics(c *fiber.Ctx) error {
	db := database.DB
	var fanfics []model.Fanfic
	db.Find(&fanfics)
	return c.JSON(fiber.Map{"status": "success", "message": "All products", "data": fanfics})
}

// GetFanfic query CreateFanfic
func GetFanfic(c *fiber.Ctx) error {
	p := new(fanficInfo)

	if err := c.QueryParser(p); err != nil {
		return err
	}
	log.Println(p.WorkID)
	db := database.DB
	var fanfic model.Fanfic
	db.Find(&fanfic, p.WorkID)
	log.Println(fanfic.ID)
	if len(fanfic.ID) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No fanfic found with ID", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Fanfic found", "data": fanfic.Info})
}

// CreateFanfic new fanfic
func CreateFanfic(c *fiber.Ctx) error {
	db := database.DB
	var FinishedFanfic ao3.Work

	Parser := ao3.Parsing
	fanfic := new(fanficInfo)
	if err := c.BodyParser(fanfic); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't create fanfic", "data": err})
	}
	FinishedFanfic, _ = Parser(fanfic.WorkID, fanfic.ChapterID, false)
	out, err := json.Marshal(FinishedFanfic)
	if err != nil {
		panic(err)
	}

	var isfanfic model.Fanfic
	db.First(&isfanfic, fanfic.WorkID)
	if isfanfic.ID == "" {
		fanficdb := model.Fanfic{
			ID:        fanfic.WorkID,
			ChapterID: fanfic.ChapterID,
			Info:      datatypes.JSON(out),
		}
		db.Create(&fanficdb)
		return c.JSON(fiber.Map{"status": "success", "message": "Created fanfic", "data": fanficdb.ID})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Created fanfic", "data": isfanfic.ID})
}

// DeleteFanfic delete product
func DeleteFanfic(c *fiber.Ctx) error {
	WorkID := c.Params("WorkID")
	db := database.DB

	var fanfic model.Fanfic
	db.First(&fanfic, WorkID)
	if fanfic.ID == "" {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No product found with ID", "data": nil})
	}
	db.Unscoped().Delete(&fanfic)
	return c.JSON(fiber.Map{"status": "success", "message": "Product successfully deleted", "data": nil})
}

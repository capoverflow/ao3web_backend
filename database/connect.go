package database

import (
	"fmt"
	"strconv"

	"gitlab.com/capoverflow/ao3web_backend/config"
	"gitlab.com/capoverflow/ao3web_backend/model"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// DB gorm connector
var DB *gorm.DB

// ConnectDB connect to db
func ConnectDB() {
	var err error
	p := config.Config("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)
	if err != nil {
		panic(err)
	}

	// DB, err = gorm.Open("postgres", fmt.Sprintf(
	// 	"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
	// 	config.Config("DB_HOST"),
	// 	port,
	// 	config.Config("DB_USER"),
	// 	config.Config("DB_PASSWORD"),
	// 	config.Config("DB_NAME"),
	// ))

	dsn := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Config("DB_HOST"),
		port,
		config.Config("DB_USER"),
		config.Config("DB_PASSWORD"),
		config.Config("DB_NAME"),
	)
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("failed to connect database")
	}

	fmt.Println("Connection Opened to Database")
	DB.AutoMigrate(&model.Fanfic{})
	fmt.Println("Database Migrated")
}

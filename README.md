# ao3web_backend

go backend for ao3web

```bash
cp .env.sample .env
# start dev db
docker run -d --rm -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword postgres 
```

example curl 

curl --location --request POST 'http://localhost:3000/api/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "identity": "ender",
    "password": "ender"
}'

curl -X POST "http://localhost:3000/api/fanfic" \                        
--header 'Authorization: Bearer ' \
-d 'WorkID=7065943' \
-d 'ChapterID=16061035' 

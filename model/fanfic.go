package model

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

// Product struct
type Fanfic struct {
	gorm.Model
	ID        string `gorm:"primaryKey"`
	ChapterID string
	Info      datatypes.JSON
}

package router

import (
	"log"

	"gitlab.com/capoverflow/ao3web_backend/config"
	"gitlab.com/capoverflow/ao3web_backend/handler"
	"gitlab.com/capoverflow/ao3web_backend/middleware"

	"github.com/gofiber/fiber/v2"
)

// SetupRoutes setup router api
func SetupRoutes(app *fiber.App) {
	// Middleware

	if config.Config("DEPLOYMENT_TYPE") == "docker-compose" {
		log.Println(config.Config("DEPLOYMENT_TYPE"))
		app.Static("/", "./public")

	} else {
		log.Println("no compose", config.Config("DEPLOYMENT_TYPE"))
	}

	api := app.Group("/api")
	api.Get("/", handler.Health)

	// Auth
	auth := api.Group("/auth")
	auth.Post("/login", handler.Login)

	// Products
	api.Get("/fanfics", handler.GetAllFanfics)

	fanfic := api.Group("/fanfic")
	fanfic.Get("/", handler.GetFanfic)
	fanfic.Post("/", middleware.Protected(), handler.CreateFanfic)
	fanfic.Delete("/:id", middleware.Protected(), handler.DeleteFanfic)
}
